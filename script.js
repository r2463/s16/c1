



/*  Javascript Functions  */

let user = {
	firstName: "John",
	lastName: "Doe",
	age: "30",
	currentJob: "Nurse",
	reasonForShift: "Coding has always been my passion. Eventhough it wasn't my course in college, I've always dreamed of pursuing it but just didn't have the capacity to do so since due to financial reasons. I finally decided to follow what path I really wanted and hopiong to contribute to the tech industry.",

	futureGoals: "Work with a company where I can show my skills, contribute, and at the same time learn more about web development."


};


function getUser(user){
	
	console.log(`Hi, my name is ${user.firstName} ${user.lastName}. I am ${user.age} years old. I'm currently working as a ${user.currentJob}. ${user.reasonForShift}. My future goal is to ${user.futureGoals}`);

}

getUser(user);




/*  If Else Statement  */
/*  Different Gigabyte allocation when subscribing to Monthly Internet Plans  */


let gigabyte = 100;


if (gigabyte == 50){
	console.log(`You are now subscribed to Silver Plan!`)
}

else if (gigabyte == 100){
	console.log(`You are now subscribed to Gold Plan!`)
}


else {
	console.log(`Please choose a valid option.`)
}




/*  Ternary Operator  */


let goldPlan; true;

goldPlan = true ? console.log(`You are now subscribed to Gold Plan!`) : console.log(`You are now subscribed to Silver Plan!`);




/*  Switch Cases  */


function pressed(){
	var text = document.getElementById("inp").value;
	var result = document.getElementById(`result`);

switch (text){

	case `Silver` : result.innerHTML = `You are now subscribed to Silver Plan!`;
	break;

	case `Gold` : result.innerHTML = `You are now subscribed to Gold Plan!`
	break;

	default: result.innerHTML = `Please choose a valid option.`;

}
}




/*  For Loop  */



let activityTitle = (`Zuitt Bootcamp Reading List`);
let i = 0;
let newActivityTitle = [];

for (i; i < activityTitle.length; i++){

	if (activityTitle[i] == "t"){
		newActivityTitle += activityTitle[i];
	}

}

	console.log (newActivityTitle);





/*  Array  */


let listOfStudents = [`John`, `Ben`, `Philip`, `Cathy`, `Royce`];
console.log(listOfStudents);


// 1. Total length of Array
console.log(listOfStudents.length);

// 2. Add new element at the beggining of Array.
listOfStudents.unshift(`Stephen`);
console.log(listOfStudents);


// 3. Add new element at the end of the Array.
listOfStudents.push(`Jane`);
console.log(listOfStudents);


// 4. Remove existing element at the beggining of an array.
const firstElement = listOfStudents.shift();
console.log(firstElement);


// 5. Remove existing element at the end of an array.
const lastElement = listOfStudents.pop();
console.log(lastElement);


// 6. Find the index of an element in an array.
let index = listOfStudents.indexOf(`Cathy`);
console.log(index);


// 7. If there are 2 identical elements in an Array, use an Array accessor to find the index number of the last instance of that element. 

listOfStudents.push(`Cathy`);;
console.log(listOfStudents);

let lastIndex = listOfStudents.lastIndexOf(`Cathy`)
console.log(lastIndex);




/*  Objects  */



let person = {

	firstName: `Robin`,
	lastName: `Hill`,
	age: 45,
	address: [`Wisconsin`, `USA`],
	friend: [`Paul`, `Andrew`, `Collin`],
	Admin: true,


	selfIntro: function() {
		console.log(`Hi, My name is ${this.firstName} ${this.lastName}. I am ${this.age} years old.`);
	
	}
};


person.selfIntro();




/*  Array Destructuring  */


function students(){
	return [`Tina`, `Clarise`, `Ellie`];

}


let [student1, student2, student3]  = students();

console.log(student1);
console.log(student2);
console.log(student3);
console.log(`${student1}, ${student2}, and ${student3} are the only students who passed the test.`);




/*  Object Destructuring  */


let employee = {

	firstName: `Lonny`,
	lastName: `Sloetjes`,
	middleName:`Davis`

}

let {firstName: employeeFirstName, lastName: employeeLastName, middleName: employeeMiddleName} = employee;

console.log(employeeFirstName);
console.log(employeeLastName);
console.log(employeeMiddleName);
console.log	(`The employee of the year award goes to ${employeeFirstName} ${employeeMiddleName} ${employeeLastName}. Congratulations!`)





/*  Arrow Function  */



let basket = ({apple, orange}) => console.log(`I like ${apple} fruits but not the ${orange} ones`);

let fruits = {

	apple: `sweet`,
	orange: `sour`

};


basket(fruits);






